> # ✨`Profile`✨
>
> - Name: Truong Nguyen Tien Quan<br>
> - Age: 18<br>
> - Address: Savanakhet<br>
> - Momo: 0961964575<br>

## `Learn Git and GitHub without any code!`

### Using the Hello World guide, you’ll create a repository, start a branch, write comments, and open a pull request.

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://docs.github.com/en/get-started/quickstart/hello-world)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://github.com/new)

## `Join GitHub Global Campus!`

Prepare for a career in tech by joining GitHub Global Campus. Global Campus will help you get the practical industry knowledge you need by giving you access to industry tools, events, learning resources and a growing student community.

![markdown](https://github.githubassets.com/images/modules/signup/gc_banner_light.png)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://education.github.com/discount_requests/student_application)

## **`Following`**

The easiest way to introduce yourself on GitHub is by creating a README in a repository about you! You can start here:

> quysoi199/README.md
>
> - 👋 Hi, I’m @quysoi199
> - 👀 I’m interested in ...
> - 🌱 I’m currently learning ...
> - 💞️ I’m looking to collaborate on ...
> - 📫 How to reach me ...

| Plugin           | README                                    |
| ---------------- | ----------------------------------------- |
| Dropbox          | [plugins/dropbox/README.md][pldb]         |
| GitHub           | [plugins/github/README.md][plgh]          |
| Google Drive     | [plugins/googledrive/README.md][plgd]     |
| OneDrive         | [plugins/onedrive/README.md][plod]        |
| Medium           | [plugins/medium/README.md][plme]          |
| Google Analytics | [plugins/googleanalytics/README.md][plga] |
